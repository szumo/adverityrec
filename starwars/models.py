import petl.io.csv

from django.db import models
from django.utils.functional import cached_property

from . import cvsstore


class PeopleDataSet(models.Model):
    data_hash = models.CharField(max_length=200, primary_key=True)
    timestamp = models.DateTimeField(auto_now=True, db_index=True)
    person_count = models.IntegerField(null=False, blank=False, default=0)

    class Meta:
        ordering = ['-timestamp']

    @property
    def path_prefix(self):
        return self.data_hash

    @property
    def path_people(self):
        return "{}/people.csv".format(self.data_hash)

    @cached_property
    def table_people(self):
        return petl.io.csv.fromcsv(cvsstore.read_file(self.path_people))

