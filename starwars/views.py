from natsort import natsort_keygen

from django.views.generic import TemplateView, ListView
from django.views import View
from django.http import HttpResponseNotFound, HttpResponseRedirect
from django.utils.functional import cached_property
from django.utils.cache import patch_cache_control, patch_response_headers

from .models import PeopleDataSet
from . import swapi

PAGE_SIZE = 10
COLUMNS = ("Name", "Height", "Mass", "Hair color", "Skin color", "Eye color", "Birth year", "Gender", "Homeworld", "Edit date")
sort_key_func = natsort_keygen()
CACHE_TIME = 31556926 # 1 year


class DatasetListing(ListView):
    template_name = "datasets.html"
    model = PeopleDataSet

    def get_context_data(self, *args, **kwargs):
        result = super().get_context_data(*args, **kwargs)
        result['page_size'] = PAGE_SIZE
        return result


class FetchView(View):

    def post(self, request, *args, **kwargs):
        swapi.fetch()
        # TODO: show progress in a popup
        return HttpResponseRedirect('/')


class CacheMixin():
    def get(self, request, *args, **kwargs):
        response = super().get(request, *args, **kwargs)
        patch_response_headers(response, CACHE_TIME)
        patch_cache_control(response, public=True)
        return response


class PeopleView(CacheMixin, TemplateView):
    template_name = "people.html"

    def get_context_data(self, data_hash, row_count):
        try:
            dataset = PeopleDataSet.objects.get(data_hash=data_hash)
        except PeopleDataSet.DoesNotExist:
            return HttpResponseNotFound()
        context = super().get_context_data()
        context['header'] = ['#'] + list(COLUMNS)
        context['dataset'] = dataset
        table = dataset.table_people.addrownumbers().head(row_count).skip(1)
        characters = list(table)
        context['row_count_next'] = row_count + PAGE_SIZE
        context['more_to_load'] = len(characters) == row_count
        context['characters'] = characters
        return context


class GroupedView(CacheMixin, TemplateView):
    template_name = "grouped.html"

    @cached_property
    def column_numbers(self):
        return [i - 1 for i in range(len(COLUMNS)) if self.request.GET.get('c{}'.format(i))]

    def get_context_data(self, data_hash):
        try:
            dataset = PeopleDataSet.objects.get(data_hash=data_hash)
        except PeopleDataSet.DoesNotExist:
            return HttpResponseNotFound()

        context = super().get_context_data()
        context['header'] = [COLUMNS[i] for i in self.column_numbers] + ['Count']
        key = [swapi.RETURN_COLUMNS[i] for i in self.column_numbers]
        if len(key) == 1:
            key = key[0]
        print(key)
        data = dataset.table_people.aggregate(key=key, aggregation=len).skip(1)
        data = data.addfield('sort_order', sort_key_func)
        data = data.sort(key='sort_order')
        data = data.cutout('sort_order')
        context['rows'] = data
        return context
