"""
Fetch data from the SWAPI.
"""

import requests
import hashlib

import petl.io.csv
import petl.io.sources

from django.utils.dateparse import parse_datetime
from django.core.files.temp import NamedTemporaryFile
from django.conf import settings

from . import cvsstore
from .models import PeopleDataSet

RETURN_COLUMNS = ("name", "height", "mass", "hair_color", "skin_color", "eye_color", "birth_year", "gender",
                  "homeworld", "date")


class SWAPIClient(object):

    def __init__(self, progress_callback=None):
        self.base_url = settings.SWAPI_URL
        self.session = requests.session()  # use one connection to avoid repeated handshakes
        self.homeworlds = {}
        self.progress_callback = progress_callback or (lambda *args: None)
        self.person_count = 0

    def get_people(self):
        """Returns a generator that yields one person at a time"""
        yield RETURN_COLUMNS
        url = self.base_url + '/api/people/'
        page = 1
        while url:
            r = self.session.get(url)
            r.raise_for_status()
            data = r.json()
            url = data['next']  # get URL for next page of results
            for person in data['results']:
                if person.get('homeworld'):
                    person['homeworld'] = self.translate_homeworld(person['homeworld'])
                if person.get('edited'):
                    person['date'] = parse_datetime(person['edited']).date()
                row = [person.get(key) for key in RETURN_COLUMNS]
                self.progress_callback('row', row)
                self.person_count += 1
                yield row
            self.progress_callback('page', page)
            page += 1

    def translate_homeworld(self, homeworld_url):
        try:
            return self.homeworlds[homeworld_url]
        except LookupError:
            r = self.session.get(homeworld_url)
            r.raise_for_status()
            data = r.json()
            self.homeworlds[homeworld_url] = result = data['name']
            return result


def fetch(progress_callback=print):
    hash = hashlib.sha256(b'v1')

    def row_hash(kind, data):
        if kind == 'row':
            hash.update(str(data).encode('utf-8'))
        else:
            progress_callback(kind, data)

    client = SWAPIClient(progress_callback=row_hash)
    people = client.get_people()
    with NamedTemporaryFile() as output:
        petl.io.csv.tocsv(people, output.name)
        digest = hash.hexdigest() # can't calculate the digest before creating the temporary CSV file
        dataset, created = PeopleDataSet.objects.get_or_create(data_hash=digest, defaults=dict(person_count=client.person_count))
        if created:
            cvsstore.write_file(dataset.path_people, output)
        else:
            dataset.person_count = client.person_count
            dataset.save()  # just update the date
