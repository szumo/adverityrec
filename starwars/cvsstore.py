"""
Store CSV files in local filesystem. Use Google Cloud Storage as a 2nd layer because Heroku filsystem is ephemeral.
"""
import json

from gcsfs import GCSFileSystem
from django.conf import settings
from django.core.files.storage import default_storage


def google_filesystem():
    if settings.GOOGLE_BUCKET and settings.GOOGLE_CREDENTIALS:
        credentials = settings.GOOGLE_CREDENTIALS
        if credentials.startswith('{'):
            credentials = json.loads(credentials)
        gcs = GCSFileSystem(token=settings.GOOGLE_CREDENTIALS)
        return gcs


def read_file(path):
    if not default_storage.exists(path):
        gcs = google_filesystem()
        if gcs is not None:
            default_storage.save(path, gcs.open("{}/{}".format(settings.GOOGLE_BUCKET, path), "rb"))
    return default_storage.open(path)


def write_file(path, file):
    default_storage.save(path, file)
    gcs = google_filesystem()
    if gcs is not None:
        gcs.put(file.name, "{}/{}".format(settings.GOOGLE_BUCKET, path))
